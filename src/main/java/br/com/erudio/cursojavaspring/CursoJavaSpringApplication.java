package br.com.erudio.cursojavaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoJavaSpringApplication {

	public static void main(String[] args) {
		System.out.println();


		SpringApplication.run(CursoJavaSpringApplication.class, args);
	}

}
